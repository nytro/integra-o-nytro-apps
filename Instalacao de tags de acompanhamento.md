# Integração de scripts de acompanhamento Nytro Apps

Para a captura das informações de carrinhos abandonados e monitoramento das páginas visitadas pelos usuários, será necessária a inserção de alguns snippets de código JavaScript nas páginas da loja virtual.

Para cada tipo de página temos um script específico, que envia informações detalhadas para o Nytro Data Collector, nosso sistema de coleta de dados.

## Script de acompanhamento geral

Em todas as páginas da loja virtual, deverá ser inserido o script de acompanhamento abaixo, apenas se alterando o valor de `CUSTOMER_ID_NYTRO` para o seu ID de cliente Nytro (pode ser consultado no painel de controle da conta):

```
<script type="text/javascript">
(function(){
  if(typeof window._nc == 'undefined') window._nc = {};
  _nc.website_id = 'CUSTOMER_ID_NYTRO';
  var _ncs = document.createElement('script');
  _ncs.type = 'text/javascript';
  _ncs.async = true;
  _ncs.src = 'https://secure.nytro.com.br/data/collector.js';
  document.getElementsByTagName('head')[0].appendChild(_ncs);
})();
</script>
```

## Página do produto

Nas páginas de produto da loja virtual, logo antes da inserção do script de acompanhamento, devemos inserir o seguinte script, alterando-se os valores de variáveis conforme os dados em sua loja virtual:

```
<script>
    _nc = {
        type: 'product',
        product_sku: SKU, // Obrigatório
        product_name: NOME_PRODUTO,  // Obrigatório
        product_price: PRECO_PRODUTO,  // Obrigatório
        product_categories: [CAT_1, CAT_2, ...],  // Opcional
        product_url: URL_PRODUTO,  // Obrigatório
        product_image_url: URL_IMAGEM_THUMBNAIL,  // Obrigatório
        customer_id: ID_CLIENTE,  // Opcional
        customer_name: NOME_CLIENTE,  // Opcional
        customer_email: EMAIL_CLIENTE  // Opcional
    };
</script>
```

## Página do carrinho de compras

Na página de carrinho de compras da loja virtual, logo antes da inserção do script de acompanhamento, devemos inserir o seguinte script, alterando-se os valores de variáveis conforme os dados em sua loja virtual:

```
<script>
    _nc = {
        type: 'cart',
        cart_items: [
            {
                'sku' => SKU,
                'qty' => QUANTIDADE,
                'name' => NOME_PRODUTO,
                'price' => PRECO_PRODUTO,
                'url' => URL_PRODUTO,
                'image' => URL_IMAGEM_THUMBNAIL
            }
            // ...
        ],
        customer_id: ID_CLIENTE,  // Opcional
        customer_name: NOME_CLIENTE,  // Opcional
        customer_email: EMAIL_CLIENTE  // Opcional
    };
</script>
```

## Página de checkout

Na página de checkout da loja virtual, logo antes da inserção do script de acompanhamento, devemos inserir o seguinte script, alterando-se os valores de variáveis conforme os dados em sua loja virtual:

```
<script>
    _nc = {
        type: 'checkout',
        checkout_items: [
            {
                'sku' => SKU,
                'qty' => QUANTIDADE,
                'name' => NOME_PRODUTO,
                'price' => PRECO_PRODUTO,
                'url' => URL_PRODUTO,
                'image' => URL_IMAGEM_THUMBNAIL
            }
            // ...
        ],
        customer_id: ID_CLIENTE,  // Opcional
        customer_name: NOME_CLIENTE,  // Opcional
        customer_email: EMAIL_CLIENTE  // Obrigatório
    };
</script>
```

#### Páginas de checkout do tipo "One Page Checkout"

Como existem alguns sistemas de loja virtual em que a página de checkout é do tipo "One Page Chcekout", onde o cadastro do cliente é realizado na mesma tela de pagamento e revisão do pedido, pode-se utilizar um método auxiliar para enviar o e-mail do cliente assim que ele for digitado em algum formulário da página.

Para isso, preencha o script dinâmico normalmente, sem a informação de e-mail do cliente, e utilize o seguinte método JavaScript para preencher posteriormente o e-mail:

```
NDC.setCustomerEmail('EMAIL_CLIENTE');
```

Assim que este método for chamado, o script dinâmico será atualizado e será reenviado ao Nytro Data Collector, fazendo assim o registro da informação corretamente.

# Página de conversão da venda, ou página de sucesso do pedido

Na página de conversão da loja virtual, logo antes da inserção do script de acompanhamento, devemos inserir o seguinte script, alterando-se os valores de variáveis conforme os dados em sua loja virtual:

```
<script>
    _nc = {
        type: 'conversion',
        conversion_items: [
            {
                'sku' => SKU,
                'qty' => QUANTIDADE,
                'name' => NOME_PRODUTO,
                'price' => PRECO_PRODUTO,
                'url' => URL_PRODUTO,
                'image' => URL_IMAGEM_THUMBNAIL
            }
            // ...
        ],
        conversion_order_id: ID_PEDIDO,
        conversion_total: TOTAL_PEDIDO,
        customer_id: ID_CLIENTE,  // Obrigatório
        customer_name: NOME_CLIENTE,  // Obrigatório
        customer_email: EMAIL_CLIENTE  // Obrigatório
    };
</script>
```

Obs: Note que neste script de conversão são obrigatórios todos os dados do cliente.
